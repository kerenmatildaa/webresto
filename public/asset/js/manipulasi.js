const menu = [
  {
      id: 1,
      gambar: "asset/burger1.png",
      namaMenu: "Chicken Muffin with Egg",
      deskMenu: "Setangkup English muffin hangat dilapisi dengan saus mayonais, daging ayam olahan yang digoreng sempurna, telur, dan keju.",
      harga: "Rp.30,000"
    },
    {
      id: 2,
      gambar: "asset/burger2.png", 
      namaMenu: "Spicy Yakiniku Burger",
      deskMenu: "Burger daging paha ayam pedas dengan nori (rumput laut) dan irisan kol, yang dilengkapi saus yakiniku dan saus Chicken yang disajikan dalam setangkup roti lembut.",
      harga: "Rp.44,000"
  },
  {
    id: 3,
    gambar: "asset/rice1.png", 
    namaMenu: "Honey Garlic Chicken Rice",
    deskMenu: "Nasi hangat dengan topping daging ayam disajikan dengan saus honey garlic.",
    harga: "Rp.16,000"
  },
  {
    id: 4,
    gambar: "asset/kentang.jpg", 
    namaMenu: "French Fries",
    deskMenu: "Kentang goreng yang renyah dan gurih dengan tambahan bumbu.",
    harga: "Rp.15,000"
  },
  {
    id: 5,
    gambar: "asset/regpiz.png", 
    namaMenu: "Spicy Italino Regular Pizza",
    deskMenu: "Chicken Pepperoni dan Sausage dengan Mozzarella di Spicy Italian base sauce.",
    harga: "Rp.70,000"
  },
  {
    id: 6,
    gambar: "asset/cheeseroll.png", 
    namaMenu: "Cheese Roll",
    deskMenu: "Keju mozzarella gurih di dalam roti empuk dengan taburan rempah khas Italia",
    harga: "Rp.30,000"
  },
  {
    id: 7,
    gambar: "asset/sprite.png", 
    namaMenu: "Sprite",
    deskMenu: "Sprite dingin. (Bisa pakai es batu).",
    harga: "Rp.10,000"
  },
  {
    id: 8,
    gambar: "asset/cola.png", 
    namaMenu: "Coca-Cola",
    deskMenu: "Coca-cola dingin. (Bisa pakai es batu).",
    harga: "Rp.10,000"
  },
  {
    id: 9,
    gambar: "asset/fanta.png", 
    namaMenu: "Fanta",
    deskMenu: "Fanta Strawberry dingin. (Bisa pakai es batu).",
    harga: "Rp.10,000"
  },
  {
    id: 10,
    gambar: "asset/blueocean.png", 
    namaMenu: "Blue Ocean",
    deskMenu: "Sirup bluberi, lemon, biji selasih, potongan jeruk nipis.",
    harga: "Rp.31,000"
  },
  {
    id: 11,
    gambar: "asset/tofcof.png", 
    namaMenu: "Tofee Cofee",
    deskMenu: "Kopi, Sirup Karamel dan Whipped Cream.",
    harga: "Rp.31,000"
  },
  {
    id: 12,
    gambar: "asset/aqua.png", 
    namaMenu: "Aqua",
    deskMenu: "Air mineral aqua. (Bisa pakai es batu).",
    harga: "Rp.31,000"
  }
]; 

//Map

const jumlahMENU = (array) => {
  const jmlItemUnsur = document.querySelector('.kolom-order h3');
  const jumlahItem = array.reduce((accumulator) => {
    return accumulator + 1;
  }, 0);
  jmlItemUnsur.innerHTML = jumlahItem;
}


const callbackMap = (item, index)=>{
  const element = document.querySelector('#menu');

  element.innerHTML += `<div class="col col-md-3 mb-3 mt-3">
                          <div class="card">
                            <img src="${item.gambar}" class="card-img-top" height="250px">
                            <div class="card-body">
                              <h5 class="card-title">${item.namaMenu}</h5>
                              <p class="card-text">${item.deskMenu}</p>
                              <p class="font-weight-bold">${item.harga}</p>
                              <a href="#" class="btn btn-outline-dark my-2 my-sm-0">Beli</a>
                            </div>
                          </div>
                       </div>`
}

menu.map(callbackMap);
jumlahMENU(menu);

//Filter

const buttonElmnt = document.querySelector('.button-cari');
buttonElmnt.addEventListener('click', ()=>{
    const hasilPencarian = menu.filter((item, index)=>{
                      const inputElmnt  = document.querySelector('.input-keyword');   
                      const namaItem    = item.namaMenu.toLowerCase();
                      const keyword     = inputElmnt.value.toLowerCase();

                      return namaItem.includes(keyword);
                      })

    document.querySelector('#menu').innerHTML = '';
    hasilPencarian.map(callbackMap);
    jumlahMENU(hasilPencarian);
})


