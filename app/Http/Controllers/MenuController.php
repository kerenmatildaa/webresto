<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\menu;

class menuController extends Controller
{
    
    public function index()
    {
        $menu = menu::all();
        return view("home", ["menu" => $menu]);
    }

     public function create(Request $request)
    {
       //-
    }

    public function edit($id)
    {
        $menu = menu::find($id);
        return view('/crud/edit', ['menu'=>$menu]);
    }

    public function update(Request $request, $id)
    {
        $menu = search::where('id',$id)->update([
            "gambar" => $request->gambar,
            "nama" => $request->nama,
            "harga" => $request->harga,
        ]);

        return redirect('/menu')->with('success', 'Data Berhasil diupdate');
    }

    public function delete($id)
    {
        $menu = search::find($id);
        $menu->delete();

        return redirect('/menu')->with('success', 'Data Berhasil dihapus');
    }
}
