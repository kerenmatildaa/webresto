!-- Footer -->
<footer class="bg-dark text-light">
      <div class="container">
        <div class="row pt-3">
          <div class="footer" class="col text-center">
            <ul><h3 style="font-weight: bold;">Foodi</h3></ul>
            <ul><a href="#" style="font-weight: bold; color: mistyrose;">Hubungi Kami</a></ul>
            <ul><a href="#" style="font-weight: bold; color: mistyrose;">Tentang Kami</a></ul>
            <ul><a href="#" style="font-weight: bold; color: mistyrose;">Layanan</a></ul>
            <ul><p>Copyright 2020 Foodi.</p></ul>
          </div>
        </div>
      </div>
</footer>